Charabanc
=========
Charabanc is mostly a playground for prototyping a C++ D-Bus library that:

* uses [standalone ASIO](https://think-async.com/Asio/AsioStandalone.html)
* uses "modern" C++ as required
* attempts to use template magic to make passing C++ types to D-Bus methods easy and efficient

It isn't:

* complete, or even working
* suitable for any use

Building
========
Clone the repository, then run:
```bash
git submodule init
git submodule update
meson build
ninja -C build
build/dbus
```

Licence
=======
Charabanc is released under the GNU Lesser General Public License
version 3. See [COPYING](COPYING) for more information.
