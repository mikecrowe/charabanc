#include <any>
#include <functional>
#include <stdexcept>
#include <tuple>
#include <map>
#include <vector>
#include <string>
#include <cassert>
#include <cstdint>
#include <cstdio>
#include <fmt/format.h>

#define TRACE_PTR(P) static_cast<const void *>((P))
#define TRACE_ITER(I) static_cast<const void *>(&(*(I)))
#define TRACE_THIS (static_cast<const void *>(this))

template<typename... Types>
inline void TRACE(Types &&...args)
{
    fmt::print(std::forward<Types>(args)...);
}

template<typename I1, typename I2>
void dump(const std::string_view label, I1 begin, I2 end)
{
    TRACE("{}\n", label);
    size_t count = 0;
    std::string line;

    TRACE("{}: ", TRACE_ITER(begin));

    while (begin != end)
    {
	const uint8_t b = *begin++;
	TRACE(" {:02x}", b);
	if (isprint(b))
	    line += b;
	else
	    line += '.';

	if ((++count % 16) == 0)
	{
	    TRACE("   {}\n", line);
	    line.clear();

	    if (begin != end)
		TRACE("{}: ", TRACE_ITER(begin));
	}
    }

    if (count % 16)
    {
	while (count++ % 16)
	{
	    TRACE("   ");
	}
	TRACE("   {}\n", line);
    }
}

void dump(const std::string_view &label, const std::vector<uint8_t> &v)
{
    dump(label, cbegin(v), cend(v));
}

class variant;

class object_path
{
    std::string m_path;
public:
    explicit object_path(std::string path)
	: m_path(std::move(path))
    {
    }

    const std::string &get() const &
    {
	return m_path;
    }

    std::string get() &&
    {
	return std::move(m_path);
    }
};

class signature
{
    std::string m_signature;
public:
    explicit signature(std::string signature)
	: m_signature(std::move(signature))
    {
//	TRACE("{}: create signature\n", TRACE_THIS);
	if (signature.size() > std::numeric_limits<uint8_t>::max())
	    throw std::runtime_error("signature too long");
    }

#if 0
    signature(const signature &other)
	: m_signature(other.m_signature)
    {
	TRACE("{}: copy signature from {}\n", TRACE_THIS, TRACE_PTR(&other));
    }

    ~signature()
    {
	TRACE("{}: destroy signature\n", TRACE_THIS);
    }
#endif

    const std::string &get() const &
    {
	return m_signature;
    }

    std::string get() &&
    {
	return std::move(m_signature);
    }

    void clear()
    {
	m_signature.clear();
    }
};

inline bool operator==(const signature &lhs, const signature &rhs)
{
    return lhs.get() == rhs.get();
}

inline bool operator!=(const signature &lhs, const signature &rhs)
{
    return lhs.get() != rhs.get();
}

// Todo work out how we can stop this from being emitted for types
// that we don't want to support such as pointers. At the moment the
// code compiles but we get a linker error.
template<typename T>
struct signature_maker {
    static std::string make_signature();
};

template< class T >
struct remove_cvref {
    typedef std::remove_cv_t<std::remove_reference_t<T>> type;
};

template< class T >
using remove_cvref_t = typename remove_cvref<T>::type;

template<typename... Types>
signature make_signature2()
{
    const std::string sig = (signature_maker<remove_cvref_t<Types>>::make_signature() + ...);
    return signature{sig};
}

template<>
struct signature_maker<bool> {
    static std::string make_signature()
    {
	return "b";
    }
};

template<>
struct signature_maker<int64_t> {
    static std::string make_signature()
    {
	return "x";
    }
};

template<>
struct signature_maker<int32_t> {
    static std::string make_signature()
    {
	return "i";
    }
};

template<>
struct signature_maker<int16_t> {
    static std::string make_signature()
    {
	return "n";
    }
};

template<>
struct signature_maker<uint64_t> {
    static std::string make_signature()
    {
	return "t";
    }
};

template<>
struct signature_maker<uint32_t> {
    static std::string make_signature()
    {
	return "u";
    }
};

template<>
struct signature_maker<uint16_t> {
    static std::string make_signature()
    {
	return "q";
    }
};

template<>
struct signature_maker<uint8_t> {
    static std::string make_signature()
    {
	return "y";
    }
};

template<>
struct signature_maker<double> {
    static std::string make_signature()
    {
	return "d";
    }
};

template<>
struct signature_maker<std::string> {
    static std::string make_signature()
    {
	return "s";
    }
};

template<>
struct signature_maker<std::string_view> {
    static std::string make_signature()
    {
	return "s";
    }
};

// This is convenient, but it means that we must be careful in variant
// to deal explicitly with const char * values otherwise we risk just
// copying a pointer that might go out of scope.
template<>
struct signature_maker<const char *> {
    static std::string make_signature()
    {
	return "s";
    }
};

template<typename... Types>
struct signature_maker<std::tuple<Types...>> {
    static std::string make_signature()
    {
	return "(" + make_signature2<Types...>().get() + ")";
    }
};

template<typename Type>
struct signature_maker<std::vector<Type>> {
    static std::string make_signature()
    {
	return "a" + make_signature2<Type>().get();
    }
};

template<typename Key, typename Value>
struct signature_maker<std::pair<Key, Value>> {
    static std::string make_signature()
    {
	return "{" + make_signature2<Key>().get() + make_signature2<Value>().get() + "}";
    }
};

template<typename Key, typename Value>
struct signature_maker<std::map<Key, Value>> {
    static std::string make_signature()
    {
	return "a{" + make_signature2<Key>().get() + make_signature2<Value>().get() + "}";
    }
};

template<>
struct signature_maker<signature> {
    static std::string make_signature()
    {
	return "g";
    }
};

template <typename T>
struct alignment_for_t;

template <typename T>
struct alignment_for_t<std::vector<T>>
{
    static const size_t alignment = 4;
};

template <typename... Types>
struct alignment_for_t<std::tuple<Types...>>
{
    static const size_t alignment = 8;
};

template <typename Key, typename Value>
struct alignment_for_t<std::pair<Key, Value>>
{
    static const size_t alignment = 8;
};

template <typename Key, typename Value>
struct alignment_for_t<std::map<Key, Value>>
{
    // The alignment is for the array, not the tuples within it.
    static const size_t alignment = 4;
};

template <typename T>
constexpr size_t alignment_for()
{
    return alignment_for_t<T>::alignment;
}

template <>
size_t alignment_for<bool>()
{
    return 1;
}

#define EMIT_ALIGNMENT_FOR_INT(N) \
    template <>					\
    size_t alignment_for<int ## N ## _t>()	\
    {						\
	return (N)/8;				\
    }

#define EMIT_ALIGNMENT_FOR_UINT(N) \
    template <>					\
    size_t alignment_for<uint ## N ## _t>()	\
    {						\
	return (N)/8;				\
    }

EMIT_ALIGNMENT_FOR_UINT(8);
EMIT_ALIGNMENT_FOR_INT(16);
EMIT_ALIGNMENT_FOR_UINT(16);
EMIT_ALIGNMENT_FOR_INT(32);
EMIT_ALIGNMENT_FOR_UINT(32);
EMIT_ALIGNMENT_FOR_INT(64);
EMIT_ALIGNMENT_FOR_UINT(64);

template <>
size_t alignment_for<double>()
{
    return 8;
}

template <>
size_t alignment_for<std::string>()
{
    return alignment_for<uint32_t>();
}

template <>
size_t alignment_for<const char *>()
{
    return alignment_for<uint32_t>();
}

template <>
size_t alignment_for<signature>()
{
    return 1;
}

template <>
size_t alignment_for<object_path>()
{
    return alignment_for<uint32_t>();
}

struct Encoder
{
    std::vector<uint8_t> m_result;

    void align_to(size_t alignment)
    {
	while (m_result.size() % alignment)
	{
	    TRACE("padding\n");
	    m_result.push_back(0);
	}
    }

    template <class T>
    void align_for(const T &value)
    {
	const size_t alignment = alignment_for<T>();
	TRACE(" aligning to {} for {}\n", alignment, typeid(T).name());
	align_to(alignment);
    }

public:
    void append(bool b)
    {
	TRACE("append bool {}\n", b);
	append(uint32_t{b});
    }

    void append(uint64_t u)
    {
	TRACE("append uint64_t {:x}\n", u);
	align_for(u);
	m_result.push_back(u & 0xff);
	m_result.push_back((u >> 8) & 0xff);
	m_result.push_back((u >> 16) & 0xff);
	m_result.push_back((u >> 24) & 0xff);
	m_result.push_back((u >> 32) & 0xff);
	m_result.push_back((u >> 40) & 0xff);
	m_result.push_back((u >> 48) & 0xff);
	m_result.push_back((u >> 56) & 0xff);
    }

    void append(uint32_t u)
    {
	TRACE("append uint32_t {:x}\n", u);
	align_for(u);
	m_result.push_back(u & 0xff);
	m_result.push_back((u >> 8) & 0xff);
	m_result.push_back((u >> 16) & 0xff);
	m_result.push_back((u >> 24) & 0xff);
    }

    void append(uint16_t u)
    {
	TRACE("append uint16_t {:x}\n", u);
	align_for(u);
	m_result.push_back(u & 0xff);
	m_result.push_back((u >> 8) & 0xff);
    }

    void append(uint8_t u)
    {
	TRACE("append uint8_t {:#04x}\n", u);
	m_result.push_back(u);
    }

    void append(int64_t i)
    {
	TRACE("append int64_t {:x}\n", i);
	append(static_cast<uint64_t>(i));
    }

    void append(int32_t i)
    {
	TRACE("append int32_t {:x}\n", i);
	append(static_cast<uint32_t>(i));
    }

    void append(int16_t i)
    {
	TRACE("append int16_t {:x}\n", i);
	append(static_cast<uint16_t>(i));
    }

    void append(double d)
    {
	TRACE("append double {}\n", d);
	append(*reinterpret_cast<uint64_t *>(&d));
    }

    void append(std::string_view sv)
    {
	TRACE("append string {}:\"{}\"\n", sv.size(), sv);
	// todo check fits in uint32_t
	append(static_cast<uint32_t>(sv.size()));
	m_result.insert(m_result.end(), sv.begin(), sv.end());
	m_result.push_back(0);
    }

    void append(const char *str)
    {
	TRACE("append C-style string string \"{}\"\n", str);
	append(std::string_view{str});
    }

    void append(signature sig)
    {
	TRACE("append signature {}:\"{}\"\n", sig.get().size(), sig.get());
	const std::string &sig_string = sig.get();
	assert(sig_string.size() <= std::numeric_limits<uint8_t>::max());
	append(static_cast<uint8_t>(sig_string.size()));
	m_result.insert(m_result.end(), sig_string.begin(), sig_string.end());
	m_result.push_back(0);
    }

    void append(const variant &v);

    template <typename... Types>
    void append(const std::tuple<Types...> &tuple)
    {
	align_for(tuple);
	std::apply([this](auto &&...args) { (append(args), ...); }, tuple);
    }

    template <typename Type>
    void append(const std::vector<Type> &array)
    {
	TRACE("Append vector of size {}\n", array.size());
	// TODO: Ensure that the size fits in a uint32_t.
	append(static_cast<uint32_t>(array.size()));
	for(const auto &element : array)
	    append(element);
    }

    template <typename Key, typename Value>
    void append(const std::pair<Key, Value> &dict_entry)
    {
	align_for(dict_entry);
	append(dict_entry.first);
	append(dict_entry.second);
    }

    template <typename Key, typename Value>
    void append(const std::map<Key, Value> &dictionary)
    {
	// TODO: Ensure size fits in a uint32_t
	append(static_cast<uint32_t>(dictionary.size()));
	for(const auto &element : dictionary)
	    append(element);
    }

    const std::vector<uint8_t> &result() const
    {
	return m_result;
    }
};

class Decoder;

// Generic version since we can't have function partial specialization
template<typename T>
struct parser_t
{
    T parse(Decoder &decoder);
};

class Decoder
{
    const std::vector<uint8_t>::const_iterator m_begin;
    const std::vector<uint8_t>::const_iterator m_end;
    std::vector<uint8_t>::const_iterator m_pos;
    const uint8_t m_alignment_offset;

    void ensure_available(size_t octets)
    {
	if (m_end - m_pos < static_cast<ssize_t>(octets))
	{
	    TRACE("Insufficient input: pos={} required={} end={}\n",
		  m_pos - m_begin,
		  octets,
		  m_end - m_begin);
	    throw std::range_error("Insufficient input");
	}
    }

    void align_to(size_t alignment)
    {
	while ((m_pos - m_begin + m_alignment_offset) % alignment)
	{
	    ++m_pos;
	    TRACE("skip byte for alignment - now at {} = {:#04x}\n", m_pos - m_begin, *m_pos);
	}
    }

    template<typename T>
    void align_for()
    {
	const size_t alignment = alignment_for<T>();
	TRACE("Alignment for {} is {}\n", typeid(T).name(), alignment);
	align_to(alignment);
    }

    explicit Decoder(std::vector<uint8_t>::const_iterator begin,
		     std::vector<uint8_t>::const_iterator end,
		     uint8_t alignment_offset)
	: m_begin(begin),
	  m_end(end),
	  m_pos(begin),
	  m_alignment_offset(alignment_offset)
    {
    }

    std::vector<uint8_t>::const_iterator current_item_end(const std::string::const_iterator sig_begin,
							  const std::string::const_iterator sig_end);

public:
    void dump(const std::string_view label)
    {
	::dump(label, m_pos, m_end);
    }

    explicit Decoder(const std::vector<uint8_t> &input)
	: m_begin(input.begin()),
	  m_end(input.end()),
	  m_pos(input.begin()),
	  m_alignment_offset(0)
    {
    }

    // Generic version since we can't have function partial specialization
    template<typename T>
    remove_cvref_t<T> parse()
    {
	parser_t<remove_cvref_t<T>> parser;
	align_for<remove_cvref_t<T>>();
	return parser.parse(*this);
    }

//    std::pair<std::vector<uint8_t>::const_iterator, std::vector<uint8_t>::const_iterator> current_item_bounds(const signature &sig)
//    std::vector<uint8_t>::const_iterator current_item_end(const signature &sig);
};

#if 0
    void append(uint64_t u)
    {
	m_result.push_back(u & 0xff);
	m_result.push_back((u >> 8) & 0xff);
	m_result.push_back((u >> 16) & 0xff);
	m_result.push_back((u >> 24) & 0xff);
	m_result.push_back((u >> 32) & 0xff);
	m_result.push_back((u >> 40) & 0xff);
	m_result.push_back((u >> 48) & 0xff);
	m_result.push_back((u >> 56) & 0xff);
    }
#endif
template <>
uint8_t Decoder::parse<uint8_t>()
{
    TRACE("parse uint8_t\n");
    align_for<uint8_t>();
    ensure_available(sizeof(uint8_t));
    uint32_t result = *m_pos++;
    return result;
}

template <>
uint16_t Decoder::parse<uint16_t>()
{
    TRACE("parse uint16_t\n");
    align_for<uint16_t>();
    ensure_available(sizeof(uint16_t));
    uint16_t result = *m_pos++;
    result |= (*m_pos++) << 8;
    return result;
}

template <>
int16_t Decoder::parse<int16_t>()
{
    TRACE("parse int16_t\n");
    return static_cast<int16_t>(parse<uint16_t>());
}

template <>
uint32_t Decoder::parse<uint32_t>()
{
    TRACE("parse uint32_t\n");
    align_for<uint32_t>();
    ensure_available(sizeof(uint32_t));
    uint32_t result = *m_pos++;
    result |= (*m_pos++) << 8;
    result |= (*m_pos++) << 16;
    result |= (*m_pos++) << 24;
    return result;
}

template <>
int32_t Decoder::parse<int32_t>()
{
    TRACE("parse int32_t\n");
    return static_cast<int32_t>(parse<uint32_t>());
}

template<>
bool Decoder::parse<bool>()
{
    TRACE("parse bool\n");
    align_for<uint8_t>();
    return parse<uint32_t>() != 0;
}

template<>
std::string Decoder::parse<std::string>()
{
    TRACE("parse string\n");
    const size_t len = parse<uint32_t>();
    TRACE("string length {}\n", len);
    ensure_available(len);

    std::string result;
    result.reserve(len);
    std::copy(m_pos, m_pos + len, std::back_inserter(result));
    // consume the trailing NUL too
    m_pos += len + 1;
    return result;
}

template<>
signature Decoder::parse<signature>()
{
    TRACE("Parse signature\n");
    const size_t len = parse<uint8_t>();
    ensure_available(len);

    std::string result;
    result.reserve(len);
    std::copy(m_pos, m_pos + len, std::back_inserter(result));

    // consume the trailing NUL too
    m_pos += len + 1;
    TRACE("  result={}:\"{}\"\n", len, result);
    return signature{result};
}

class variant
{
    // For correct alignment in the ultimate message, we need to store
    // the signature and contents separately. (TODO Is it worth
    // storing the alignment, or should we just call
    // alignment_for_signature?)
    signature m_signature;
    size_t m_alignment;
    std::vector<uint8_t> m_contents;

public:
    explicit variant(signature &&sig, size_t alignment,
		     std::vector<uint8_t>::const_iterator begin,
		     std::vector<uint8_t>::const_iterator end)
	: m_signature(std::move(sig)),
	  m_alignment(alignment),
	  m_contents(begin, end)
    {
	assert(!m_signature.get().empty());
	TRACE("Created variant for signature '{}'\n", m_signature.get());
	dump("variant contents from iterator", m_contents);
    }

    template<typename T>
    explicit variant(T value)
	: m_signature(make_signature2<T>()),
	  m_alignment(alignment_for<T>())
    {
	assert(!m_signature.get().empty());
	Encoder encoder;
	encoder.append(value);
	m_contents = encoder.result();
	dump("variant contents from type", m_contents);
    }

    const signature &get_signature() const
    {
	return m_signature;
    }

    const std::vector<uint8_t> &get_contents() const
    {
	return m_contents;
    }

    size_t get_alignment() const
    {
	TRACE("alignment for variant:{} = {}\n", m_signature.get(), m_alignment);
	return m_alignment;
    }

    template <typename T>
    T get() const
    {
	assert(make_signature2<T>() == m_signature);
	Decoder decoder(m_contents);

	return decoder.parse<remove_cvref_t<T>>();
    }
};

#if 0
template <typename T>
T variant_cast(const variant &v)
{
    assert(make_signature2<T>() == v.get_signature());
    Decoder decoder(v.get_contents());

    return decoder.parse<remove_cvref_t<T>>();
}
#endif

template<>
struct signature_maker<variant> {
    static std::string make_signature()
    {
	return "v";
    }
};

void Encoder::append(const variant &v)
{
    TRACE("append variant of type {}\n", v.get_signature().get());
    append(v.get_signature());
    align_to(v.get_alignment());
    TRACE("XXX\n");
    dump("before append variant value", m_result);
    m_result.insert(m_result.end(), v.get_contents().begin(), v.get_contents().end());
    dump("after append variant value", m_result);
}

inline size_t alignment_for_signature(const signature &sig)
{
    TRACE("alignment for signature \"{}\"\n", sig.get());
    assert(!sig.get().empty());
    switch (sig.get().front())
    {
    case 'y':
    case 'g':
    case 'v':
	return 1;
    case 'n':
    case 'q':
	return 2;
    case 'b':
    case 'i':
    case 'u':
    case 's':
    case 'o':
    case 'a':
	return 4;
    case 'x':
    case 't':
    case 'd':
    case 'r':
    case '(':
    case 'e':
    case '{':
	return 8;
    default:
	assert(false);
    }
}

template<>
variant Decoder::parse<variant>()
{
    TRACE("Parse variant\n");
    auto sig = parse<signature>();
    TRACE(" variant signature='{}'\n", sig.get());

    const size_t alignment = alignment_for_signature(sig);
    while ((m_pos - m_begin + m_alignment_offset) % alignment)
	++m_pos;

    const auto data_begin = m_pos;
    const auto data_end = current_item_end(std::cbegin(sig.get()), std::cend(sig.get()));
    m_pos = data_end;
    return variant(std::move(sig), alignment, data_begin, data_end);
}

#if 0
// Scan across a signature looking for the matching bracket
std::string::const_iterator sig_scan(const std::string::const_iterator sig_begin,
				     const std::string::const_iterator sig_end,
				     const std::string_view delimiters)
{
    int depth = 1;
    assert(delimiters.size() == 2);
    std::string::const_iterator sig_pos = sig_begin;
    while (depth != 0)
    {
	sig_pos = std::find_first_of(sig_pos, sig_end, std::cbegin(delimiters), std::cend(delimiters));
	if (sig_pos == sig_end)
	    throw std::range_error("Truncated signature");
	else if (*i == delimiters.back())
	    --depth;
	else if (*i == delimiters.front())
	    ++depth;
	else
	    assert(false);
    }
}
#else
std::string::const_iterator sig_bracket_scan(const std::string::const_iterator sig_begin,
					     const std::string::const_iterator sig_end)
{
    assert(sig_begin != sig_end);

    std::string::const_iterator sig_pos = sig_begin;
    std::string stack;
    stack.push_back(*sig_pos++);

    while (!stack.empty() && (sig_pos != sig_end))
    {
	//TRACE("Stack='{}' current='{}'\n", stack, *sig_pos);
	switch (*sig_pos)
	{
	case '(':
	case '{':
	    stack.push_back(*sig_pos);
	    break;
	case ')':
	    assert(!stack.empty());
	    if (stack.back() != '(')
		throw std::runtime_error("Unexpected ')' in signature");
	    stack.pop_back();
	    break;
	case '}':
	    assert(!stack.empty());
	    if (stack.back() != '{')
		throw std::runtime_error("Unexpected '}' in signature");
	    stack.pop_back();
	    break;
	}
	++sig_pos;
    }

    if (!stack.empty())
	throw std::range_error("Truncated signature");
    return sig_pos;
}
#endif

std::string::const_iterator sig_next(const std::string::const_iterator sig_begin,
				     const std::string::const_iterator sig_end)
{
    std::string::const_iterator sig_pos = sig_begin;
    if (sig_pos == sig_end)
	throw std::range_error("Truncated signature");

    switch (*sig_pos)
    {
    case 'y':
    case 's':
    case 'g':
    case 'v':
    case 'i':
    case 'u':
    case 'n':
    case 'q':
    case 'b':
    case 'o':
    case 'x':
    case 't':
    case 'd':
	return ++sig_pos;
    case 'a':
	return sig_next(++sig_pos, sig_end);
    case '{':
	return sig_bracket_scan(sig_pos, sig_end);
    case '(':
	return sig_bracket_scan(sig_pos, sig_end);
    default:
	assert(false);
    }
}

// It's not possible to determine the data size of a variant without
// looking at the data itself
std::vector<uint8_t>::const_iterator Decoder::current_item_end(const std::string::const_iterator sig_begin,
							       const std::string::const_iterator sig_end)
{
    TRACE("finding current item end for {} from {}\n", std::string_view(&*sig_begin, sig_end - sig_begin), TRACE_ITER(m_pos));
    Decoder sub(m_pos, m_end, (m_pos - m_begin + m_alignment_offset) % 8);
    auto sig_pos = sig_begin;

    if (sig_pos != sig_end)
    {
	TRACE("  considering item {}\n", *sig_pos);
	const char sig_char = *sig_pos++;
	switch (sig_char)
	{
	case 'y':
	    sub.parse<uint8_t>();
	    break;
	case 's':	    TRACE("before string pos = {}\n", TRACE_ITER(sub.m_pos));
	    sub.parse<std::string>();
	    TRACE("after string pos = {}\n", TRACE_ITER(sub.m_pos));
	    break;
	case 'g':
	    sub.parse<signature>();
	    break;
	case 'v':
	    sub.parse<variant>();
	case 'i':
	    sub.parse<int32_t>();
	    break;
	case 'u':
	    sub.parse<uint32_t>();
	    break;
	case 'n':
	case 'q':
	case 'b':
	case 'o':
	case 'a':
	{
	    sub.dump("left");
	    TRACE("before array pos = {}\n", TRACE_ITER(sub.m_pos));
	    uint32_t count = sub.parse<uint32_t>();
	    TRACE(" array has {} elements\n", count);
	    if (sig_pos == sig_end)
		throw std::range_error("Truncated signature");
	    while (count--)
	    {
		TRACE("before element pos = {}\n", TRACE_ITER(sub.m_pos));
		// TODO: This doesn't work if the array has anything other than 1 element (including zero.)
		sub.m_pos = sub.current_item_end(sig_pos, sig_end);
	    }
	    sig_pos = sig_next(sig_pos, sig_end);
	    TRACE("after elements pos = {}\n", TRACE_ITER(sub.m_pos));
	    break;
	}
	case '{':
	case '(':
	{
	    sub.align_to(8); // DictEntry alignment
	    const auto terminator = (sig_char == '{') ? '}' : ')';
	    sub.dump("left");
	    while (*sig_pos != terminator)
	    {
		TRACE("checking {} against terminator={}\n", *sig_pos, terminator);
		if (sig_pos == sig_end)
		    throw std::range_error("Truncated signature");
		sub.m_pos = sub.current_item_end(sig_pos, sig_end);
		sig_pos = sig_next(sig_pos, sig_end);
	    }
	    // skip terminator
	    ++sig_pos;
	    break;
	}
	case 'x':
	case 't':
	case 'd':
	case 'r':
	case 'e':
	default:
	    assert(false);
	}
    }

    TRACE(" item is between {} and {} aka {} and {}\n", m_pos - m_begin, sub.m_pos - m_begin, TRACE_ITER(m_pos), TRACE_ITER(sub.m_pos));
    return sub.m_pos;
}

template<typename T>
struct parser_t<std::vector<T>>
{
    std::vector<T> parse(Decoder &decoder)
    {
	TRACE("parse vector\n");
	uint32_t len = decoder.parse<uint32_t>();
	TRACE("  length={}\n", len);
	std::vector<T> result;
	result.reserve(len);
	while (len--)
	    result.push_back(decoder.parse<remove_cvref_t<T>>());
	return result;
    }
};

template<typename... Args>
struct parser_t<std::tuple<Args...>>
{
    std::tuple<Args...> parse(Decoder &decoder)
    {
	// Use initializer list to ensure that the elements are parsed
	// in the right order.
	std::tuple<Args...> result{decoder.parse<Args>()...};
	return result;
    }
};

template<typename Key, typename Value>
struct parser_t<std::map<Key, Value>>
{
    std::map<Key, Value> parse(Decoder &decoder)
    {
	TRACE("Parse map\n");
	std::map<Key, Value> result;
	uint32_t len = decoder.parse<uint32_t>();
	TRACE("  length={}\n", len);
	while (len--)
	{
	    const auto entry = decoder.parse<std::tuple<Key, Value>>();
	    result.emplace(std::get<0>(entry), std::get<1>(entry));
	}
	return result;
    }
};

template <typename Return, typename... Args>
Return parse_to_callback(const signature &signature_in,
			 const std::vector<uint8_t> &input,
			 Return (&callback)(Args...))
{
    auto const signature_built = make_signature2<Args...>();
    if (signature_built != signature_in)
	throw std::runtime_error("Signature mismatch");

    Decoder decoder(input);
    std::tuple<remove_cvref_t<Args>...> args{decoder.parse<remove_cvref_t<Args>>()...};
    return std::apply(callback, args);
}

template <typename Return, typename... Args>
Return parse_to_callback(const std::pair<signature, std::vector<uint8_t>> &input,
			 Return (&callback)(Args...))
{
    TRACE("Enter parse_to_callback\n");
    auto const signature_built = make_signature2<Args...>();
    if (signature_built != input.first)
	throw std::runtime_error("Signature mismatch");

    TRACE("Decoding from signature '{}'\n", input.first.get());
    Decoder decoder(input.second);
    std::tuple<remove_cvref_t<Args>...> args{decoder.parse<remove_cvref_t<Args>>()...};
    return std::apply(callback, args);
}

template<typename F, typename T, typename U>
decltype(auto) apply_invoke(F&& func, T&& first, U&& tuple) {
    return std::apply(std::forward<F>(func),
		      std::tuple_cat(std::forward_as_tuple(std::forward<T>(first)),
				     std::forward<U>(tuple)));
}

template <typename T, typename Return, typename... Args>
Return parse_to_memfn(const std::pair<signature, std::vector<uint8_t>> &input,
		      T &obj,
		      Return (T::*callback)(Args...))
{
    TRACE("Enter parse_to_memfn\n");
    auto const signature_built = make_signature2<Args...>();
    if (signature_built != input.first)
	throw std::runtime_error(fmt::format("Signature mismatch: passed='{}', args='{}'",
					     input.first.get(), signature_built.get()));

    TRACE("Decoding from signature '{}'\n", input.first.get());
    Decoder decoder(input.second);
    std::tuple<remove_cvref_t<Args>...> args{decoder.parse<remove_cvref_t<Args>>()...};
    return apply_invoke(callback, obj, args);
}

// Unfortunately this must be passed the types because we can't
// determine them from the callable
template <typename... Args, typename Callable>
std::invoke_result_t<Callable, Args...> parse_to_callable(const std::pair<signature, std::vector<uint8_t>> &input,
			 Callable &&callback)
{
    TRACE("Enter parse_to_callback\n");
    auto const signature_built = make_signature2<Args...>();
    if (signature_built != input.first)
	throw std::runtime_error("Signature mismatch");

    TRACE("Decoding from signature '{}'\n", input.first.get());
    Decoder decoder(input.second);

    // Must be a braced initializer in order to guarantee that the
    // parse calls occur left to right. See
    // https://en.cppreference.com/w/cpp/language/eval_order rule 10.
    std::tuple<remove_cvref_t<Args>...> args{decoder.parse<remove_cvref_t<Args>>()...};
    return std::apply(callback, args);
}

template <typename Callback>
struct helper;

template <typename TReturn, typename... TArgs>
struct helper<TReturn (TArgs...)>
{
    using Return = TReturn;
    using ArgsTuple = std::tuple<TArgs...>;
};

template <typename... Args>
std::pair<signature, std::vector<uint8_t>> encode(Args... args)
{
    auto signature = make_signature2<Args...>();

    Encoder encoder;
    (encoder.append(args), ...);

    return std::make_pair(signature, encoder.result());
}

template <typename... Args>
void call(const std::string_view method, Args... args)
{
//    auto signature = (make_signature<Args>() + ...);
    auto signature = make_signature2<Args...>();
    TRACE("Signature: {}\n", signature.get());

    Encoder encoder;
    (encoder.append(args), ...);
    dump("call", encoder.result());
}

void print_it(bool b, uint32_t u)
{
    printf("%d %u\n", b, u);
}

int main3()
{
//    signature signature;
//    std::vector<uint8_t> data;
    auto [ signature, data ] = encode(false, 42U);
    TRACE("Signature: {}\n", signature.get());
    dump("main3", data);

#if 0
    parse_to_callback(signature, data, [](bool b, uint32_t u) -> void {
					   printf("%d, %u\n", b, u);
				       });
#else
    parse_to_callback(signature, data, print_it);
#endif
    return 0;
}

int test_call()
{
    call("hello", 42, 42, static_cast<uint8_t>(7), "Goodbye");
    call("hello", std::make_tuple(42, "Hello"));
    call("hello", std::make_tuple(42, std::make_tuple("Hello", std::make_tuple(7U, 3.141))));

    call("ints64", uint64_t{0xfeedf00ddeadbeef}, int64_t(9223372036854775807));
    call("ints32", uint32_t{0xfeedbeef}, int32_t(2147483647));
    call("ints16", uint16_t{0xfeed}, int16_t(32767));
    call("byte", uint8_t{0xfe});
    call("bool", true);

    std::vector<int> vi;
    vi.push_back(42);
    vi.push_back(43);
    call("hello", vi);

    std::vector<std::tuple<std::string, uint8_t>> vt;
    vt.push_back(std::make_tuple("Hello", 85));
    vt.push_back(std::make_tuple("Tuple", 21));
    call("goodbye", vt);

    std::vector<std::tuple<int32_t, std::vector<std::tuple<std::string, uint8_t>>>> vtvt;
    vtvt.push_back(std::make_tuple(12345678, vt));
    call("wibble", vtvt);

    call("dictentry", std::make_pair("key", "value"), std::make_pair("key", 66));

    std::map<std::string, std::tuple<int32_t, int32_t, int32_t>> dict;
    dict["test"] = std::make_tuple(42, 42, 42);
    dict["another"] = std::make_tuple(85, 85, 85);
    call("dictionary", dict);

    call("signature", signature("iyiy"));

    return 0;
}

//#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

using namespace Catch::Matchers;

TEST_CASE("Signature parsing")
{
    SECTION("Simple signature")
    {
	signature sig{"i"};
	CHECK(sig_next(std::cbegin(sig.get()), std::cend(sig.get())) == std::cend(sig.get()));
    }

    SECTION("Longer signature")
    {
	signature sig{"gy"};
	const auto sig_begin = std::cbegin(sig.get());
	const auto sig_end = std::cend(sig.get());
	CHECK(sig_next(sig_begin, sig_end) == sig_begin + 1);
	CHECK(sig_next(sig_begin + 1, sig_end) == sig_end);
    }

    SECTION("Struct")
    {
	signature sig{"(iyx)!"};
	const auto sig_begin = std::cbegin(sig.get());
	const auto sig_end = std::cend(sig.get());
	CHECK(sig_next(sig_begin, sig_end) == sig_end - 1);
    }

    SECTION("Dict")
    {
	signature sig{"{is}!"};
	const auto sig_begin = std::cbegin(sig.get());
	const auto sig_end = std::cend(sig.get());
	CHECK(sig_next(sig_begin, sig_end) == sig_end - 1);
    }

    SECTION("Array")
    {
	signature sig{"as!"};
	const auto sig_begin = std::cbegin(sig.get());
	const auto sig_end = std::cend(sig.get());
	CHECK(sig_next(sig_begin, sig_end) == sig_end - 1);
    }

    SECTION("Array of dict")
    {
	signature sig{"a{is}"};
	const auto sig_begin = std::cbegin(sig.get());
	const auto sig_end = std::cend(sig.get());
	CHECK(sig_next(sig_begin, sig_end) == sig_end);
    }

    SECTION("Compound")
    {
	signature sig{"(asas{ia{s(oa{xg})}})!"};
	const auto sig_begin = std::cbegin(sig.get());
	const auto sig_end = std::cend(sig.get());
	CHECK(sig_next(sig_begin, sig_end) == sig_end - 1);
    }

    SECTION("Mismatch braces")
    {
	SECTION("(}")
	{
	    signature sig{"(asas}!"};
	    const auto sig_begin = std::cbegin(sig.get());
	    const auto sig_end = std::cend(sig.get());
	    CHECK_THROWS_WITH(sig_next(sig_begin, sig_end), "Unexpected '}' in signature");
	}

	SECTION("})")
	{
	    signature sig{"a{hh)!"};
	    const auto sig_begin = std::cbegin(sig.get());
	    const auto sig_end = std::cend(sig.get());
	    CHECK_THROWS_WITH(sig_next(sig_begin, sig_end), "Unexpected ')' in signature");
	}
    }

    SECTION("Truncated")
    {
	SECTION("Array")
	{
	    signature sig{"a"};
	    const auto sig_begin = std::cbegin(sig.get());
	    const auto sig_end = std::cend(sig.get());
	    CHECK_THROWS_WITH(sig_next(sig_begin, sig_end), "Truncated signature");
	}

	SECTION("Struct")
	{
	    SECTION("Empty")
	    {
		signature sig{"("};
		const auto sig_begin = std::cbegin(sig.get());
		const auto sig_end = std::cend(sig.get());
		CHECK_THROWS_WITH(sig_next(sig_begin, sig_end), "Truncated signature");
	    }

	    SECTION("Non-empty")
	    {
		signature sig{"(h"};
		const auto sig_begin = std::cbegin(sig.get());
		const auto sig_end = std::cend(sig.get());
		CHECK_THROWS_WITH(sig_next(sig_begin, sig_end), "Truncated signature");
	    }
	}

	SECTION("DictEntry")
	{
	    SECTION("Empty")
	    {
		signature sig{"{"};
		const auto sig_begin = std::cbegin(sig.get());
		const auto sig_end = std::cend(sig.get());
		CHECK_THROWS_WITH(sig_next(sig_begin, sig_end), "Truncated signature");
	    }

	    SECTION("Non-empty")
	    {
		signature sig{"{h"};
		const auto sig_begin = std::cbegin(sig.get());
		const auto sig_end = std::cend(sig.get());
		CHECK_THROWS_WITH(sig_next(sig_begin, sig_end), "Truncated signature");
	    }
	}
    }
}

void variant_handler(const std::vector<variant> &v)
{
    REQUIRE(v.size() == 4);

    std::vector<std::string> sigs_received;
    std::transform(std::cbegin(v), std::cend(v), std::back_inserter(sigs_received),
		   [](const variant &v)
		   {
		       return v.get_signature().get();
		   });
    const std::vector<std::string> sigs_expected = { "y", "s", "as", "a{s(iii)}" };
    REQUIRE(sigs_received == sigs_expected);

    CHECK(v[0].get<uint8_t>() == 42);
    CHECK(v[1].get<std::string>() == "a longer string to defeat the short string optimisation");
    CHECK_THAT(v[2].get<std::vector<std::string>>(), Equals<std::string>({"One", "Two", "Three is long" }));

    const auto dict = v[3].get<std::map<std::string, std::tuple<int32_t, int32_t, int32_t>>>();
    CHECK(dict.size() == 2);
    CHECK(dict.at("test") == std::make_tuple(42, 42, 42));

}

TEST_CASE("Round-trip")
{
    SECTION("Byte")
    {
	auto encoded = encode(static_cast<uint8_t>('Z'));
	CHECK(encoded.first.get() == "y");
	CHECK(parse_to_callable<uint8_t>(encoded, [](uint8_t b) {
						       CHECK(b == 'Z');
						       return true;
						   }));
    }

    SECTION("uint16")
    {
	SECTION("Alignment zero")
	{
	    auto encoded = encode(static_cast<uint16_t>(6565));
	    CHECK(encoded.first.get() == "q");
	    CHECK(parse_to_callable<uint16_t>(encoded, [](uint16_t u) {
								CHECK(u == 6565);
								return true;
							    }));
	}

	SECTION("Alignment one")
	{
	    auto encoded = encode(uint8_t{'?'}, static_cast<uint16_t>(6565));
	    CHECK(encoded.first.get() == "yq");
	    CHECK(parse_to_callable<uint8_t, uint16_t>(encoded, [](uint8_t b, uint16_t u) {
									  CHECK(b == '?');
									  CHECK(u == 6565);
									  return true;
								      }));
	}
    }

    SECTION("int16")
    {
	SECTION("Alignment zero")
	{
	    auto encoded = encode(static_cast<int16_t>(-6565));
	    CHECK(encoded.first.get() == "n");
	    CHECK(parse_to_callable<int16_t>(encoded, [](int16_t i) {
								CHECK(i == -6565);
								return true;
							    }));
	}

	SECTION("Alignment one")
	{
	    auto encoded = encode(uint8_t{'?'}, int16_t{-32767});
	    CHECK(encoded.first.get() == "yn");
	    CHECK(parse_to_callable<uint8_t, int16_t>(encoded, [](uint8_t b, int16_t i) {
									  CHECK(b == '?');
									  CHECK(i == -32767);
									  return true;
								      }));
	}
    }

    SECTION("Integer")
    {
	SECTION("Alignment zero")
	{
	    auto encoded = encode(42);
	    CHECK(encoded.first.get() == "i");
	    CHECK(parse_to_callable<int32_t>(encoded, [](int32_t i) {
								CHECK(i == 42);
								return true;
							    }));
	}

	SECTION("Alignment one")
	{
	    auto encoded = encode(static_cast<uint8_t>('A'), 4242);
	    CHECK(encoded.first.get() == "yi");
	    dump("encoded", encoded.second);
	    CHECK(parse_to_callable<uint8_t, int32_t>(encoded, [](uint8_t b, int32_t i) {
									 CHECK(b == 'A');
									 CHECK(i == 4242);
									 return true;
								     }));
	}

	SECTION("Alignment two")
	{
	    auto encoded = encode(uint16_t{1212}, 424242);
	    CHECK(encoded.first.get() == "qi");
	    dump("encoded", encoded.second);
	    CHECK(parse_to_callable<uint16_t, int32_t>
		  (encoded, [](uint16_t w, int32_t i) {
				CHECK(w == 1212);
				CHECK(i == 424242);
				return true;
			    }));
	}

	SECTION("Alignment three")
	{
	    auto encoded = encode(uint16_t{1313}, uint8_t{'B'}, 42424242);
	    CHECK(encoded.first.get() == "qyi");
	    dump("encoded", encoded.second);
	    CHECK(parse_to_callable<uint16_t, uint8_t, int32_t>
		  (encoded, [](uint16_t w, uint8_t b, int32_t i) {
				CHECK(w == 1313);
				CHECK(b == 'B');
				CHECK(i == 42424242);
				return true;
			    }));
	}

	SECTION("Alignment four")
	{
	    auto encoded = encode(uint32_t{14141414}, 4242424242U);
	    CHECK(encoded.first.get() == "uu");
	    dump("encoded", encoded.second);
	    CHECK(parse_to_callable<uint32_t, uint32_t>
		  (encoded, [](uint32_t u1, uint32_t u2) {
				CHECK(u1 == 14141414);
				CHECK(u2 == 4242424242);
				return true;
			    }));
	}
    }

    SECTION("String")
    {
	auto encoded = encode("Too long for short string optimisation");
	CHECK(encoded.first.get() == "s");
	CHECK(parse_to_callable<std::string>
	      (encoded, [](const std::string &str) {
			    CHECK(str == "Too long for short string optimisation");
			    return true;
			}));
    }

    SECTION("Signature")
    {
	SECTION("Alignment zero")
	{
	    auto encoded = encode(signature{"(iiiasasasasasasasas)"});
	    CHECK(encoded.first.get() == "g");
	    TRACE("About to parse\n");
	    CHECK(parse_to_callable<signature>(encoded, [](const signature &sig) {
							    CHECK(sig == signature{"(iiiasasasasasasasas)"});
							    return true;
							}));
	}

	SECTION("Alignment one")
	{
	    auto encoded = encode(uint16_t{4242}, signature{"(iiiasasasasasasasas)"});
	    CHECK(encoded.first.get() == "qg");
	    TRACE("About to parse\n");
	    CHECK(parse_to_callable<uint16_t, signature>(encoded,
							     [](uint16_t w, const signature &sig)
							     {
								 CHECK(w == 4242);
								 CHECK(sig == signature{"(iiiasasasasasasasas)"});
								 return true;
							     }));
	}
    }

    SECTION("Array")
    {
	std::string value;
	std::vector<std::string> send_vec;
	value.reserve(100);
	send_vec.reserve(100);
	while (send_vec.size() < 100)
	{
	    send_vec.push_back(value);
	    value += "x";
	}

	SECTION("Alignment zero")
	{
	    auto encoded = encode(send_vec);
	    REQUIRE(encoded.first.get() == "as");
	    CHECK(parse_to_callable<std::vector<std::string>>(encoded,
							      [&send_vec](const std::vector<std::string> &receive_vec) {
								  CHECK(send_vec == receive_vec);
								  return true;
							      }));
	}

	SECTION("Alignment one")
	{
	    auto encoded = encode(uint8_t{'A'}, send_vec);
	    REQUIRE(encoded.first.get() == "yas");
	    CHECK(parse_to_callable<uint8_t, std::vector<std::string>>
		  (encoded,
		   [&send_vec](uint8_t b, const std::vector<std::string> &receive_vec) {
		       CHECK(b == 'A');
		       CHECK(send_vec == receive_vec);
		       return true;
		   }));
	}
    }

    SECTION("Tuple")
    {
	const auto send_tuple = std::make_tuple(std::string{"Tuple member one"},
						static_cast<uint8_t>(85),
						static_cast<uint32_t>(12345));
	SECTION("Alignment zero")
	{
	    auto encoded = encode(send_tuple);
	    CHECK(encoded.first.get() == "(syu)");
	    CHECK(parse_to_callable<decltype(send_tuple)>
		  (encoded,
		   [&send_tuple](const std::tuple<std::string, uint8_t, uint32_t> &receive_tuple) {
		       CHECK(send_tuple == receive_tuple);
		       return true;
		   }));
	}

	SECTION("Alignment one")
	{
	    auto encoded = encode(uint8_t{'x'}, send_tuple);
	    CHECK(encoded.first.get() == "y(syu)");
	    CHECK(parse_to_callable<uint8_t, decltype(send_tuple)>
		  (encoded,
		   [&send_tuple](uint8_t b, const std::tuple<std::string, uint8_t, uint32_t> &receive_tuple) {
		       CHECK(b == 'x');
		       CHECK(send_tuple == receive_tuple);
		       return true;
		   }));
	}
    }

    SECTION("Dict")
    {
	std::map<std::string, int32_t> send_map = {
	    { "first dictionary entry", 1 },
	    { "second dictionary entry", 2 },
	    { "third dictionary entry", 3 }
	};

	SECTION("Alignment zero")
	{
	    auto encoded = encode(send_map);
	    CHECK(encoded.first.get() == "a{si}");
	    dump("encoded dict", encoded.second);
	    CHECK(parse_to_callable<std::map<std::string, int32_t>>
		  (encoded, [&send_map](const std::map<std::string, int32_t> &received_map) {
				CHECK(send_map == received_map);
				return true;
			    }));
	}

	SECTION("Alignment one")
	{
	    auto encoded = encode(uint8_t{'*'}, send_map);
	    CHECK(encoded.first.get() == "ya{si}");
	    dump("encoded dict", encoded.second);
	    CHECK(parse_to_callable<uint8_t, std::map<std::string, int32_t>>
		  (encoded, [&send_map](uint8_t b, const std::map<std::string, int32_t> &received_map) {
				CHECK(b == '*');
				CHECK(send_map == received_map);
				return true;
			    }));
	}
    }

    SECTION("Member")
    {
	struct C
	{
	    std::string m_string;
	    std::vector<uint32_t> m_vector;

	    void Callback(const std::string &str, const std::vector<uint32_t> &vec)
	    {
		m_string = str;
		m_vector = vec;
	    }
	};

	C c;
	std::vector<uint32_t> vec{ 1, 2, 3, 4 };
	std::string_view str("Pass string to member function");
	auto encoded = encode(str, vec);
	CHECK(encoded.first.get() == "sau");
	parse_to_memfn(encoded, c, &C::Callback);
	CHECK(vec == c.m_vector);
	CHECK(str == c.m_string);
    }

    SECTION("Variant")
    {
	std::map<std::string, std::tuple<int32_t, int32_t, int32_t>> dict;
	dict["test"] = std::make_tuple(42, 42, 42);
	dict["another"] = std::make_tuple(85, 85, 85);

	std::vector<std::string> strings;
	strings.emplace_back("One");
	strings.emplace_back("Two");
	strings.emplace_back("Three is long");

	std::vector<variant> variants;
	variants.push_back(variant{uint8_t{42}});
	variants.push_back(variant{"a longer string to defeat the short string optimisation"});
	variants.emplace_back(strings);
	variants.emplace_back(dict);

	auto encoded = encode(variants);
	TRACE("encoded signature: {}\n", encoded.first.get());
	dump("test_variant", encoded.second);

	parse_to_callback(encoded,
			  variant_handler);
    }
}
